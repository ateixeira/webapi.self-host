﻿using Microsoft.Owin.Hosting;
using System;
using System.Configuration;
using System.ServiceProcess;

namespace hgest.api
{
    public class Program
    {
        static void Main()
        {
#if DEBUG
            string baseAddress = ConfigurationManager.AppSettings["binding"];
            using (WebApp.Start<Startup>(url: baseAddress))
            {
                Console.WriteLine("Server is listening...");
                Console.ReadLine();
            }
#else
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new HGestService()
            };
            ServiceBase.Run(ServicesToRun);
#endif
        }
    }
}