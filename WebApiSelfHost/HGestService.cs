﻿using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace hgest.api
{
    partial class HGestService : ServiceBase
    {
        public HGestService()
        {
            InitializeComponent();
        }

        private IDisposable webApp;

        protected override void OnStart(string[] args)
        {
            string baseAddress = ConfigurationManager.AppSettings["binding"];
            webApp = WebApp.Start<Startup>(url: baseAddress);
        }

        protected override void OnStop()
        {
            webApp.Dispose();
        }
    }
}
