﻿var app;
(function (app) {
    "use strict";

    var baseUrl = '';

    function getJson(url, doneCallback, failCallback) {
        return $.ajax({
            type: 'GET',
            url: baseUrl + url
        }).done(doneCallback).fail(failCallback || app.showError);
    };

    app.test = function () {
        getJson('/api/values', function (data) {
            $("#result tbody").empty();
            $.each(data, function (i, item) {
                var $tr = $('<tr>').append(
                    $('<td>').text(item)
                ).appendTo('#result tbody');
            });
        });
    }
})(app || (app = {}));