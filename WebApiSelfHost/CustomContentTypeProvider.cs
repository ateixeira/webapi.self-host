﻿using Microsoft.Owin.StaticFiles.ContentTypes;

namespace hgest.api
{
    public class CustomContentTypeProvider : FileExtensionContentTypeProvider
    {
        public CustomContentTypeProvider()
        {
            Mappings.Add(".json", "application/json");
        }
    }
}
